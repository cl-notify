(in-package :cl-notify)

(defmethod (setf hint) (value (n notification) key type)
  (let ((cobj (slot-value n 'c-object)))
    
    (ecase type
      (:int
       (notify-notification-set-hint-int32 cobj key value))
      
      (:double
       (notify-notification-set-hint-double cobj key value))

      (:string
       (notify-notification-set-hint-string cobj key value))

      (:byte
       (notify-notification-set-hint-byte cobj key value))

      (:byte-array
       (with-foreign-object (array :uchar (length value))
         (loop
            for x in value
            for i upfrom 0
            do (setf (mem-aref array :uchar i) x))

         (notify-notification-set-hint-byte-array cobj key
                                                  array (length value)))))
    
    (values)))