(in-package :cl-notify)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; The library ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-foreign-library libnotify
    (:unix (:or "libnotify.so.1" "libnotify.so"))
    (t (:default "libnotify")))

(use-foreign-library libnotify)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Types ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defctype gboolean :int)
(defctype NotifyNotification :pointer)

(defcenum NotifyUrgency
  :low :normal :critical)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Glib ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defcfun "g_object_unref" :void (object :pointer))
(defcfun "g_free" :void (mem :pointer))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; libnotify ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defcfun ("notify_init" notify_init) gboolean (name :string))

(defcfun "notify_notification_new" NotifyNotification
  (summary :string) (body :string) (icon :string)
  (attach :pointer))

(defcfun "notify_notification_set_timeout" :void
  (n NotifyNotification)
  (timeout :int))

(defcfun "notify_notification_show" gboolean
  (n NotifyNotification)
  (err :pointer))

(defcfun "notify_notification_update" gboolean
  (n NotifyNotification)
  (summary :string) (body :string) (icon :string))

(defcfun "notify_get_server_info" gboolean
  (name :pointer)
  (vendor :pointer)
  (version :pointer)
  (spec-version :pointer))

(defcfun "notify_notification_set_urgency" :void
  (n NotifyNotification)
  (u NotifyUrgency))

;; libnotify hints


(defcfun "notify_notification_set_hint_int32" :void
  (n NotifyNotification)
  (key :string)
  (value :int))

(defcfun "notify_notification_set_hint_double" :void
  (n NotifyNotification)
  (key :string)
  (value :double))

(defcfun "notify_notification_set_hint_string" :void
  (n NotifyNotification)
  (key :string)
  (value :string))

(defcfun "notify_notification_set_hint_byte" :void
  (n NotifyNotification)
  (key :string)
  (value :uchar))

(defcfun "notify_notification_set_hint_byte_array" :void
  (n NotifyNotification)
  (key :string)
  (value :pointer)
  (len :int))

(defcfun "notify_notification_clear_hints" :void
  (n NotifyNotification))