;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Notes ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This is a binding to libnotify, wrapping some of the functionality.
;;
;; Not wrapped yet:
;; ‣ GTK specific stuff, such as:
;;   • The GtkWidget* attach argument in notify_notification_new
;;   • notify_notification_new_with_status_icon (takes a GtkStatusIcon*)
;;   • notify_notification_attach_to_widget (takes a GtkWidget*)
;;   • notify_notification_attach_to_status_icon (takes a GtkStatusIcon*)
;;   • notify_notification_set_geometry_hints (takes a GtkScreen*)
;;   • notify_notification_set_icon_from_pixbuf (takes a GdkPixbuf*)
;;
;; ‣ Callback actions
;;   • notify_notification_add_action
;;   • notify_notification_clear_actions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cl-notify)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Public Interface ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass notification ()
  ((summary  :initarg  :summary)
   (body     :initarg  :body)
   (icon     :initarg  :icon    :initform nil)
   (category :initform nil)
   (urgency  :initform :normal)

   (c-object :initform nil)))

(defgeneric show-notification (notification))
(defgeneric close-notification (notification))


;;;;;;;;;;;;;;;;;;;;;;; Generic class and library funcs. ;;;;;;;;;;;;;;;;;;;;;;;

(defun notify-init ()
  "Initialize libnotify."
  (if (= 0 (notify_init "cl-notify")) nil t))

(defmethod initialize-instance :after ((n notification)
                                       &key summary body icon)
  (setf (slot-value n 'c-object)
        (notify-notification-new summary
                                 (or body (null-pointer))
                                 (or icon (null-pointer))
                                 (null-pointer)))
  (when (null-pointer-p (slot-value n 'c-object))
    (error "Could not create notification object.")))


;;;;;;;;;;;;;;;;;;;; Creating and destroying notifications ;;;;;;;;;;;;;;;;;;;;;

(defun destroy-notification (n)
  "Unref the C object in the background. Consider using `with-notification'."

  (unless (null-pointer-p (slot-value n 'c-object))
    (g-object-unref (slot-value n 'c-object))))

(defmacro with-notification (sym (summary &key body icon timeout urgency)
                             &body forms)
  "Evaluate the forms in FORMS with SYM bound to a notification object created
  with a summary of SUMMARY and a body of BODY. If ICON, TIMEOUT and/or URGENCY
  are set, use them too."
  `(let ((,sym (make-instance 'notification
                              :summary ,summary
                              :body ,body
                              :icon ,icon)))
     (unwind-protect
          (progn

            ,(when timeout
                   `(setf (timeout ,sym) ,timeout))
            ,(when urgency
                   `(setf (urgency ,sym) ,urgency))

            ,@forms)

       (destroy-notification ,sym))))

(defmacro dispatch-notification (summary &key body icon timeout urgency)
  "Create and display a notification. For more complicated cases when you need
the symbol to stick around, use `with-notification'."
  `(with-notification n (,summary
                         ,@(when body (list :body body))
                         ,@(when icon (list :icon icon))
                         ,@(when timeout (list :timeout timeout))
                         ,@(when urgency (list :urgency urgency)))
     (show-notification n)))

(defmethod show-notification ((n notification))
  (unless (= 1 (notify-notification-show (slot-value n 'c-object)
                                         (null-pointer)))
    (error "Unknown error. TODO: Read the GError!"))
  (values))

;;;;;;;;;;;;;;;;;;;;; Modify attributes of a notification. ;;;;;;;;;;;;;;;;;;;;;


(defmethod (setf timeout) (timeout (n notification))
  (notify-notification-set-timeout (slot-value n 'c-object)
                                   timeout))

(defmethod update-notification ((n notification)
                                &key summary body icon
                                (re-show t))
  "Update the notification N with a new summary, body and/or icon. If RE-SHOW,
  then call show-notification afterwards to make the changes visible."

  (when (= 0 (notify-notification-update (slot-value n 'c-object)
                                         (or summary (slot-value n 'summary))
                                         (or body (null-pointer))
                                         (or icon (null-pointer))))
    (error "Couldn't update the notification."))
  (when re-show
    (show-notification n)))

(defmethod (setf summary) (summary (n notification)
                           &optional (re-show t))
  (update-notification n :summary summary :re-show re-show))

(defmethod (setf body) (body (n notification)
                        &optional (re-show t))
  (update-notification n :body body :re-show re-show))

(defmethod (setf icon) (icon (n notification)
                        &optional (re-show t))
  (update-notification n :icon icon :re-show re-show))

(defmethod (setf urgency) (urgency (n notification))
  (notify-notification-set-urgency (slot-value n 'c-object)
                                   urgency))

;;;;;;;;;;;;;;;;;;;; General server/connection information. ;;;;;;;;;;;;;;;;;;;;

(defmacro with-foreign-string-pointers (syms freeing-func &body body)
  "Execute BODY with the symbols in the list SYMS bound to foreign pointers,
  each of which has been set to null. Once BODY has finished, release any
  strings that aren't null with FREEING-FUNC. Evaluate to (progn BODY)."

  `(with-foreign-objects
       ,(loop
           for s in syms
           collecting `(,s :pointer))

     ,@(loop
          for s in syms
          collecting `(setf (mem-ref ,s :pointer) (null-pointer)))

     (prog1
         (progn ,@body)

       ,@(loop
            for s in syms
            collecting `(unless (null-pointer-p (mem-ref ,s :pointer))
                          (funcall ,freeing-func
                                   (mem-ref ,s :pointer)))))))

(defun server-info ()
  (with-foreign-string-pointers
      (name vendor version spec-version)
      #'g-free
    
    (notify-get-server-info name vendor version spec-version)
    
    (mapcar (lambda (pptr)
              (foreign-string-to-lisp (mem-ref pptr :pointer)))
            (list name vendor version spec-version))))