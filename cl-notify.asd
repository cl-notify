(defsystem cl-notify
  :name "cl-notify"
  :author "Rupert Swarbrick"
  :licence "GPLv3+"
  :description "Simple binding to libnotify."
  :version "0.0.1"

  :depends-on (:cffi)

  :components ((:file "package")
               (:file "bindings" :depends-on ("package"))
               (:file "cl-notify" :depends-on ("package" "bindings"))
               (:file "hints" :depends-on ("package" "cl-notify"))))
