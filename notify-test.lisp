(defpackage :notify-test
  (:use :cl :cl-notify))
(in-package :notify-test)

(notify-init)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Tests ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;; Test 1: Generate a notification ;;;;;;;;;;;;;;;;;;;;;;;;

(dispatch-notification "Summary"
                       :body "Some rather longer content. Blah Blah Blah."
                       :timeout 3000)

;;;;;;;;;;;;;;;;;; Test 2: Callbacks (not implemented) ;;;;;;;;;;;;;;;;;;

;; See test-default-action.c and test-multi-actions.c

(warn "Test 2: Not implemented.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Test 3: Images ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; First create a notification with a stock icon
(dispatch-notification "Icon Test"
                       :body "Testing stock icon"
                       :icon "appointment-new"
                       :timeout 3000)

;; Now try an icon by uri. This assumes that the working directory is set to the
;; cl-notify source directory.

(dispatch-notification "Own icon test"
                       :body "Thank you, Conrad Barski!"
                       :icon (format nil "file://~A"
                                     (namestring (merge-pathnames
                                                  "lisplogo_warning_128.png"))))

(warn "Test 3: We don't know enough to play with pixbufs yet.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Test 4: Markup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(dispatch-notification "Markup test"
                       :body "<b>bold</b>, <u>underlined</u>, <i>italic</i>,
<a href='http://www.google.com'>linked</a> text"
                       :timeout 3000)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Test 5: Replace ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(with-notification n
    ("Replace me!" :timeout 5000)
  (show-notification n)
  (sleep 2)
  (setf (summary n nil) "Replaced")
  (setf (body n) "Here's some new body text."))

;;;;;;;;;;;;;;;;;;;;;;;; Test 6: Attaching to widgets. ;;;;;;;;;;;;;;;;;;;;;;;;;

;; See test-replace-widget.c

(warn "Attachment to widgets not implemented.")

;;;;;;;;;;;;;;;;;;;;;;;;; Test 7: Getting server info ;;;;;;;;;;;;;;;;;;;;;;;;;;
(destructuring-bind (name vendor version specversion)
    (server-info)
  (format t "Server info~%~%~:{~A:~1,15T~A~%~}"
          `(("Name" ,name)
            ("Vendor" ,vendor)
            ("Version" ,version)
            ("Spec. Version" ,specversion))))

;; See test-server-info.c
(warn "get-server-caps not implemented: avoiding GList.")

(dispatch-notification "Low"
                       :body "Put the washing out"
                       :urgency :low)
(dispatch-notification "Medium"
                       :body "Drink coffee!"
                       :urgency :normal)
(dispatch-notification "High"
                       :body "Plug your laptop in!!"
                       :urgency :critical)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; Test 8: Setting a hint ;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(with-notification n ("X, Y Test"
                      :body "This notification should point to 150, 10")

  (setf (hint n "x" :int) 150)
  (setf (hint n "y" :int) 150)

  (show-notification n))

