(defpackage cl-notify
  (:use :cl :cffi)
  (:export :notify-init
           :with-notification
           :dispatch-notification
           :show-notification
           :update-notification
           :close-notification
           :summary
           :body
           :icon
           :server-info
           :hint))